<?php

class Controller extends AbstractController {
	
	function display() {
		// Create View
		$view = new View("import/display.html");
		
		// Display View
		$view->show();
	}
}
