<?php

class Controller extends AbstractController {
	
	function display() {
		// Get Parameters
		$filter = Form::get_str("filter");
		$group_by = Form::get_str("group_by");
		$min_count = Form::get_int("min_count");
		$order_by = Form::get_str("order_by");
		$start_date = Form::get_str("start_date");
		$end_date = Form::get_str("end_date");
		
		// Log Activity
		logg(" [*] Analysis");
		
		// Create View
		$analysis = new Analysis();
		$view_model = $analysis->generate_parameters($start_date, $end_date, $filter, $group_by, $min_count, $order_by);
		$view = new View("analysis/display.html", $view_model);
		
		// Display View
		$view->show();
	}
	
	function add_rule() {
		// Create Rule
		$rule = new Rule();
		
		// Populate Attributes
		$rule->pattern = Form::get_str("pattern");
		$rule->category_id = Form::get_int("category");
		
		// Log Activity
		logg(" * Adding Rule");
		logg(" - Pattern: '{$rule->pattern}'");
		logg(" - Category: #{$rule->category_id}");
		
		// Save Rule
		$rule->save();
		
		// Process Rules
		$rule->run();
		
		// Redirect
		MVC::redirect("analysis", "display");
	}
	
}
