<?php

class Analysis {
	
	private $categories;
	
	public function __construct() {
		$this->categories = array();
	}
	
	public function generate_parameters($start_date="", $end_date="", $filter="", $group_by="uid", $min_count="1", $order_by="date") {
		// Log Activity
		logg(" - Creating view model for Analysis.", 6);
		
		// Local Variables
		$summary = array();
		$expenses = 0;
		$income = 0;
		
		// Set Defaults
		$group_by = (strlen($group_by))? $group_by : "uid";
		$min_count = ($min_count > 0)? $min_count : 0;
		$order_by = (strlen($order_by))? $order_by : "date";
		$start_date = (strlen($start_date))? $start_date : date("Y-m-d", mktime(0, 0, 0, date("m") - 1, date("d"), date("Y")));
		$end_date = (strlen($end_date))? $end_date : date("Y-m-d");
		
		// Log Filter Criteria
		logg("  > Start Date: " . $start_date);
		logg("  > End Date: " . $end_date);
		logg("  > Group By: " . $group_by);
		logg("  > Order By: " . $order_by);
		logg("  > Min Count: " . $min_count);
		
		// Get Listing Data
		$query = "	SELECT
						`id`,
						`category_id`,
						`date` as 'Date',
						`description` as 'Description',
						`amount` as 'Amount',
						COUNT(*) as 'Count'
					FROM
						`statement_lines`
					WHERE
						`active` = 1
						AND `date` BETWEEN '{$start_date} 00:00:00' AND '{$end_date} 23:59:59'
						{$filter}
					GROUP BY
						{$group_by}
					HAVING
						Count > {$min_count}
					ORDER BY
						{$order_by}
				";
		$data = MVC::DB()->fetch($query);
		
		// Create Lisitng
		$listing = "";
		foreach ($data as $item) {
			$listing .= "
			<tr>
				<td>{$item->Date}</td>
				<td>{$item->Description}</td>
				<td align='right'>" . $this->format_currency($item->Amount) . "</td>
				<td>{$item->Count}</td>
				<td>" . $this->category_badge($item->uid, $item->category_id, $item->Description) . "</td>
			</tr>
			";
			
			// Update Summary
			$value = (isset($summary[$item->category_id]))? $summary[$item->category_id] : 0;
			$summary[$item->category_id] = $value + $item->Amount;
			if ($item->Amount > 0) {
				$income = $income + $item->Amount;
			}
			else {
				$expenses = $expenses + $item->Amount;
			}
		}
		
		// Sort Summary Array
		$s = array();
		foreach ($summary as $key => $val) {
			$s[] = $key;
		}
		for ($i = 0; $i < sizeof($s) + 1; $i++) {
			for ($j = $i + 1; $j < sizeof($s); $j++) {
				$key1 = $s[$i];
				$key2 = $s[$j];
				if ($summary[$key2] < $summary[$key1]) {
					$tmp = $s[$i];
					$s[$i] = $s[$j];
					$s[$j] = $tmp;
				}
			}
		}
		$ordered_summary = array();
		foreach($s as $key) {
			$ordered_summary[] = array($key, $summary[$key]);
		}
		
		// Create Summary Listing
		$summary_listing = "";
		foreach ($ordered_summary as $arr) {
			$key = $arr[0];
			$value = $arr[1];
			$category = new Category($key);
			$summary_listing .= "
			<tr>
				<td>{$category->name}</td>
				<td align='right'>" . $this->format_currency($value) . "</td>
			</tr>
			";
		}
		
		// Compile Array
		$arr = array();
		$arr['listing'] = $listing;
		$arr['category_select'] = MVC::General()->select_box("category", "Category", "id", "name", null, null, "name");
		$arr['summary'] = $summary_listing;
		$arr['income'] = $this->format_currency($income);
		$arr['expenses'] = $this->format_currency($expenses);
		$arr['start_date'] = $start_date;
		$arr['end_date'] = $end_date;
		$arr['group_by'] = $group_by;
		$arr['order_by'] = $order_by;
		$arr['min_count'] = $min_count;
		
		// Return Array
		return $arr;
	}
	
	public function category_badge($line_id, $category_id, $description) {
		if (!$category_id) {
			return "<a onclick='show_new_rule_modal(\"{$description}\");' style='cursor:pointer;'>Create Rule</a>";
		}
		
		if (!isset($this->categories[$category_id])) {
			$this->categories[$category_id] = new Category($category_id);
		}
		
		return $this->categories[$category_id]->name;
	}
	
	public function format_currency($value) {
		return "R " . number_format($value, 2);
	}
	
}
