<?php

class User extends Model {

	public $username; // (String)
	public $password; // (String)
	public $first_name; // (String)
	public $last_name; // (String)
	public $email; // (String)

	public function __construct($uid=0) {
		$this->uid_field = "id";
		$this->id = $uid;
		$this->table = "users";
		$this->load();
	}

}

