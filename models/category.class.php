<?php

class Category extends Model {
	
	public $name; // (String)
	public $colour; // (String)
	
	public function __construct($uid=0) {
		$this->uid_field = "id";
		$this->id = $uid;
		$this->table = "categories";
		$this->load();
	}

}
