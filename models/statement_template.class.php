<?php

class StatementTemplate extends Model {

	public $name; // (String)
	public $num_ignore_top; // (Integer[3])
	public $num_ignore_bottom; // (Integer[3])
	public $column_date; // (Integer[1])
	public $column_description; // (Integer[1])
	public $column_amount; // (Integer[1])
	public $column_balance; // (Integer[1])
	public $field_separator; // (String[2])
	
	public function __construct($uid=0) {
		$this->uid_field = "id";
		$this->id = $uid;
		$this->table = "statement_templates";
		$this->load();
	}

	public static function get_default() {
		// Get first Template associated with the current user
		$factory = new StatementTemplate();
		$items = $factory->get("active = 1, created_by = " . get_user_uid());
		
		// Return Item
		return (isset($items[0]))? $items[0] : new StatementTemplate();
	}

}
