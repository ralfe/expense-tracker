<?php

class Importer {

	public function Import($file, $template_id) {
		// Log Activity
		logg(" * Importing File '{$file}' using Template #{$template_id}"); 
		
		// Get Statement Template
		$template = ($template_id)? new StatementTemplate($template_id) : StatementTemplate::get_default();
		
		// Create new Statement Object
		$statement = new Statement();
		$statement->file = $file;
		$statement->creation_date = date("Y-m-d H:i:s");
		$statement->active = 1;
		$statement->save();
		
		// Get Line Items
		$line_items = $this->parse_statement($statement, $template);
		
		// Save Lines
		foreach ($line_items as $item) {
			$item->statement_id = $statement->id;
			$item->creation_date = date("Y-m-d H:i:s");
			$item->active = 1;
			$item->save();
		}
	}
	
	public function parse_statement($statement, $template) {
		// Create array of lines
		$all_lines = explode("\n", trim(file_get_contents($statement->file)));
		
		// Extract only the lines we are interested in
		$offset = $template->num_ignore_top;
		$length = sizeof($all_lines) - $template->num_ignore_top - $template->num_ignore_bottom;
		$lines = array_slice($all_lines, $offset, $length);
		
		// Process Lines
		$statement_lines = array();
		foreach ($lines as $line) {
			$statement_lines[] = $this->parse_line($line, $template);
		}
		
		// Return Statement Lines
		return $statement_lines;
	}
	
	public function parse_line($raw_line, $template) {
		// Break down line
		$data = explode($template->field_separator, $raw_line);
		
		// Create Statement Line Object
		$line = new StatementLine();
		
		// Populate Parameters from Line
		$line->date = MVC::General()->format_mysql_date($data[$template->column_date - 1]);
		$line->description = $data[$template->column_description - 1];
		$line->amount = $data[$template->column_amount - 1];
		$line->balance = $data[$template->column_balance - 1];
		
		// Log Debugging data
		logg("  > Parsing line: " . $raw_line, 8);
		logg("   : Date = " . $line->date, 10);
		logg("   : Description = " . $line->description, 10);
		logg("   : Amount = " . $line->amount, 10);
		logg("   : Balance = " . $line->balance, 10);
		
		// Return Statement Line
		return $line;
	}

}
