<?php

class Statement extends Model {

	public $file; // (String)
	public $account_id; // (Account)

	public function __construct($uid=0) {
		$this->uid_field = "id";
		$this->id = $uid;
		$this->table = "statements";
		$this->load();
	}

}

