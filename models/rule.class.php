<?php

class Rule extends Model {
	
	public $pattern; // (String)
	public $category_id; // (Category)
	
	public function __construct($uid=0) {
		$this->uid_field = "id";
		$this->id = $uid;
		$this->table = "rules";
		$this->load();
	}

	public function run() {
		// Log Activity
		logg(" [*] Running Rule");
		logg(" - Pattern: " . $this->pattern);
		logg(" - Category: " . $this->category_id);
		
		// Construct Query
		$query = "	UPDATE
						`StatementLine`
					SET
						`category_id` = '{$this->category_id}'
					WHERE
						`active` = 1
						AND `created_by` = '" . get_user_uid() . "'
						AND `description` LIKE \"{$this->pattern}\"
						AND `category_id` = 0
				";
		
		// Execute Query
		MVC::DB()->query($query);
	}
	
	public static function run_all() {
		// Log Activity
		logg(" [*] Running all Rules");
		
		// Get All Rules
		$factory = new Rule();
		$items = $factory->get("active = 1, created_by = " . get_user_uid());
		
		// Run Rules
		foreach ($items as $rule) {
			$rule->run();
		}
	}
	
}

