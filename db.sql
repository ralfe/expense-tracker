CREATE DATABASE `money`;
USE `money`;
GRANT ALL PRIVILEGES ON `money`.* TO 'money'@'localhost' IDENTIFIED BY 'm0N3y123';

CREATE TABLE `categories` (
    `id` int(11) auto_increment,
    `name` varchar(255),
    `colour` varchar(16),
    `created_by` int(11),
    `creation_date` datetime,
    `active` int(1) NOT NULL default 0,
    PRIMARY KEY (`id`)
);

CREATE TABLE `rules` (
    `id` int(11) auto_increment,
    `pattern` varchar(255),
    `category_id` int(11),
    `created_by` int(11),
    `creation_date` datetime,
    `active` int(1) NOT NULL default 0,
    PRIMARY KEY (`id`)
);

CREATE TABLE `accounts` (
    `id` int(11) auto_increment,
    `created_by` int(11),
    `creation_date` datetime,
    `active` int(1) NOT NULL default 0,
    PRIMARY KEY (`id`)
);

CREATE TABLE `statements` (
    `id` int(11) auto_increment,
    `file` varchar(255),
    `account_id` int(11),
    `created_by` int(11),
    `creation_date` datetime,
    `active` int(1) NOT NULL default 0,
    PRIMARY KEY (`id`)
);

CREATE TABLE `statement_lines` (
    `id` int(11) auto_increment,
    `statement_id` int(11),
    `date` date,
    `description` varchar(255),
    `amount` decimal(10,2),
    `balance` decimal(10,2),
    `created_by` int(11),
    `creation_date` datetime,
    `active` int(1) NOT NULL default 0,
    PRIMARY KEY (`id`)
);

CREATE TABLE `users` (
    `id` int(11) auto_increment,
    `username` varchar(255),
    `email` varchar(255),
    `password` varchar(255),
    `created_by` int(11),
    `creation_date` datetime,
    `active` int(1) NOT NULL default 0,
    PRIMARY KEY (`id`)
);

CREATE TABLE `statement_templates` (
    `id` int(11) auto_increment,
    `name` varchar(255),
    `num_ignore_top` int(5),
    `num_ignore_bottom` int(5),
    `column_date` int(5),
    `column_description` int(5),
    `column_amount` int(5),
    `column_balance` int(5),
    `field_separator` varchar(10),
    `created_by` int(11),
    `creation_date` datetime,
    `active` int(1) NOT NULL default 0,
    PRIMARY KEY (`id`)
);

INSERT INTO `statement_templates` (`name`, `num_ignore_top`, `num_ignore_bottom`, `column_date`, `column_description`, `column_amount`, `column_balance`, `field_separator`) VALUES ('Nedbank', 4, 1, 1, 2, 3, 4, ',');
