
function show_new_rule_modal(pattern) {
	$("#new_pattern").val(pattern);
	$("#new_rule_modal").modal("show");
}

function add_rule() {
	// Get Variables
	var pattern = $("#new_pattern").val();
	var category = $("#category").val();
	
	// Create new Rule
	var url = "?p=analysis&action=add_rule";
	url += "&category=" + category;
	url += "&pattern=" + encodeURI(pattern);
	window.location.href = url;
}

